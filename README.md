
DogeCash (DOGEC) an open source cryptocurrency and globally accessible payment network, with a highly-scalable, hyper-fast, low-fee transaction profile. The DogeCash developers network features a low-barrier, fully democratized governance mechanism, user-friendly cross-platform clients for universal adoption and a large-scale masternode network supporting a diverse set of services available to network participants. To secure its network, DogeCash utilizes a Quark-based Proof of Work consensus algorithm, to be later switched over to an environmentally-friendly Proof of Stake. Masternode operators are compensated for the services they provide, as are miners and stakers for cryptographically validating transaction on the network.

For more information please visit us at: [dogecash.org](https://dogecash.org/) or visit our ANN thread at: [BitcoinTalk](https://bitcointalk.org/index.php?topic=2343884.0)

# Coin Specs

• Coin Name: DogeCash  
• Ticker: DOGEC  
• PoW Algorithm: Quark  
• Premine: (#1 Block) 100,001 DogeCash (0,17% of PoW)*  
• PoW Blocks: 2 - 409000  
• PoS Blocks: Starting from 409001  
• Block Time: 60 Seconds
• PoW Max Coin Output/Supply: 57,879,300  
• PoW Ending: ~ ca. 330 Days (Estimated: October 2018)  
• Masternode Requirements: 10,000 DOGEC  
• Maturity: 30 Confirmations  
• Prefix: DogeCash adresses start with the capital letter "A"   

*the premine is going to be burned on the 1st November 2018

# PoW Reward Distribution

<table>
  <tr><th>Block Height</th><th>Reward Amount</th><th>Masternodes</th><th>Miners</th><th>Dev fee</th></tr>
  <tr><td>Block 2 - 86400</td><td>200 DOGEC</td><td>   Masternodes 40 DOGEC</td><td>Miners 160 DOGEC</td><td>0 DOGEC</td></tr>
<tr><td>Block 86401 - 151200</td><td>150 DOGEC</td><td>  Masternodes 37,5 DOGEC</td><td>Miners 112,5 DOGEC</td><td>0 DOGEC</td></tr>
<tr><td>Block 151201 - 225000</td><td>125 DOGEC</td><td>  Masternodes 37,5 DOGEC</td><td>Miners 87,5 DOGEC</td><td>0 DOGEC</td></tr>
<tr><td>Block 225001 - 302400</td><td>125 DOGEC</td><td>  Masternodes 75 DOGEC</td><td>Miners 43,75 DOGEC</td><td>6,25 DOGEC</td></tr>
<tr><td>Block 302401 - 345600</td><td>100 DOGEC</td><td>   Masternodes 60 DOGEC</td><td>Miners 35 DOGEC</td><td>5 DOGEC</td></tr>
<tr><td>Block 345601 - 388800</td><td>75 DOGEC</td><td>      Masternodes 45 DOGEC</td><td>Miners 26,25 DOGEC</td><td>3,75 DOGEC</td></tr>
<tr><td>Block 388801 - 408960</td><td>50 DOGEC</td><td>      Masternodes 30 DOGEC</td><td>Miners 17,5 DOGEC</td><td>2,5 DOGEC</td></tr>
<tr><td>Block 408961 - 409000</td><td>1 DOGEC</td><td>      Masternodes 0.6 DOGEC</td><td>Miners 0.35 DOGEC</td><td>0.5 DOGEC</td></tr>
</table>

Masternode and staking rewards with PoS:
During the PoS phase, rewards will be distributed among masternode operators, stakers and the dev fund based on a distribution ratio of 50%/40%/10% respectively.

# PoS Reward Distribution

_Proof of Stake will automatically be enabled at block height 409001._
<table>
<tr><th>Block Height</th><th>Reward Amount</th>
<tr><td>Block 409001 - 410400</td><td>1 DOGEC</td></tr>
<tr><td>Block 410401 - 540000</td><td>60 DOGEC</td></tr>
<tr><td>Block 540001 - 583200</td><td>25 DOGEC</td></tr>
<tr><td>Block 583201 - 626400</td><td>10 DOGEC</td></tr>
<tr><td>Block 626401 - infinite</td><td>5 DOGEC</td></tr>
</table>


# Further information

_For more information check out our whitepaper at: [dogecash.org/whitepaper.pdf](https://dogecash.org/whitepaper.pdf)_


Alternatively, we have added a non .pdf version in our Github repository. You can find it in the DogeCash Github Wiki.

[https://github.com/DogeCashCRYPTO/DogeCash/wiki](https://github.com/DogeCashCRYPTO/DogeCash/wiki)